<?php

if($_POST){
	/*echo '<pre>';
	var_dump($_POST);
	echo '</pre>';
	exit();*/
	
	if(empty($_POST['fecha1'])){
		echo '<div class="alert alert-danger">La Fecha n° 1 está vacía</div>';
	}
	elseif(empty($_POST['fecha2'])){
		echo '<div class="alert alert-danger">La Fecha n° 2 está vacía</div>';
	}
	elseif(empty($_POST['fecha3'])){
		echo '<div class="alert alert-danger">La Fecha n° 3 está vacía</div>';
	}
	elseif(empty($_POST['fecha4'])){
		echo '<div class="alert alert-danger">La Fecha n° 4 está vacía</div>';
	}
	elseif(empty($_POST['numero1'])){
		echo '<div class="alert alert-danger">El número 1 está vacío</div>';
	}
	elseif(empty($_POST['numero2'])){
		echo '<div class="alert alert-danger">El número 2 está vacío</div>';
	}
	elseif(empty($_POST['numero3'])){
		echo '<div class="alert alert-danger">El número 3 está vacío</div>';
	}
	elseif(empty($_POST['numero4'])){
		echo '<div class="alert alert-danger">El número 4 está vacío</div>';
	}
	else{
		$fecha1 = $_POST['fecha1'];
		$numero1 = $_POST['numero1'];
		
		$fecha2 = $_POST['fecha2'];
		$numero2 = $_POST['numero2'];
		
		$fecha3 = $_POST['fecha3'];
		$numero3 = $_POST['numero3'];
		
		$fecha4 = $_POST['fecha4'];
		$numero4 = $_POST['numero4'];
		
		$fecha1aComparar = strtotime($fecha1);
		$fecha2aComparar = strtotime($fecha2);
		$fecha3aComparar = strtotime($fecha3);
		$fecha4aComparar = strtotime($fecha4);
		
		
		if($fecha1aComparar >= $fecha2aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 2 debe ser mayor a la fecha 1</div>';
		}
		elseif($fecha2aComparar >= $fecha3aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 3 debe ser mayor a la fecha 2</div>';
			
		}
		elseif($fecha3aComparar >= $fecha4aComparar){
			
			echo '<div class="alert alert-danger">La Fecha n° 4 debe ser mayor a la fecha 3</div>';
			
		}
		else{
			
			include_once('funcion.php');
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(1, $fecha1, $numero1);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(2, $fecha2, $numero2);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(3, $fecha3, $numero3);
			echo '</div>';
			
			echo '<div class="col-lg-5 col-lg-offset-1">';
				agregarDiasHabiles(4, $fecha4, $numero4);
			echo '</div>';
			
		}
	}
}	